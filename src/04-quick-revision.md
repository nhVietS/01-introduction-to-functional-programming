## What does Functional Programming mean? { data-transition="fade none" }

###### Quick Revision

<pre><code data-trim data-noescape class="lang-java">
int sum(List&lt;int&gt; list) {
  var r = 0;
  for(var i = 0; i &lt; list.length; i = i + 1) {
    r = r + list[i];
  }
  return r;
}
</code></pre>

* Let's add the numbers in a list
* Are we functional programming?

<aside class="notes">
  * We set <code>r = 0</code> and also <code>i = 0</code> then <em>modify</em> these values
</aside>

---

## What does Functional Programming mean? { data-transition="fade none" }

###### Quick Revision

<pre><code data-trim data-noescape class="lang-java">
int sum(List&lt;int&gt; list) {
  var <mark>r = 0</mark>;
  for(var <mark>i = 0</mark>; i &lt; list.length; i = i + 1) {
    r = r + list[i];
  }
  return r;
}
</code></pre>

* We observe that:
  * <code>r = 0</code>
  * <code>i = 0</code>

---

## What does Functional Programming mean? { data-transition="fade none" }

###### Quick Revision

<pre><code data-trim data-noescape class="lang-java">
int sum(List&lt;int&gt; list) {
  var r = 0;
  for(var i = 0; <mark>0</mark> &lt; list.length; i = <mark>0</mark> + 1) {
    <mark>r</mark> = <mark>0</mark> + list[i];
  }
  return <mark>0</mark>;
}
</code></pre>

* We substitute the expressions <code>r</code> and <code>i</code> with their value
* Obviously, we are <strong>not</strong> functional programming here

---

## What does Functional Programming mean? { data-transition="fade none" }

###### Quick Revision

<pre><code data-trim data-noescape class="lang-java">
int sum(List&lt;int&gt; list) {
  if(list.isEmpty)
    return 0;
  else {
    val f = list.first;
    val r = list.rest;
    return f + sum(r);
  }
}
</code></pre>

---

## What does Functional Programming mean? { data-transition="fade none" }

###### Quick Revision

<pre><code data-trim data-noescape class="lang-java">
int sum(List&lt;int&gt; list) {
  if(list.isEmpty)
    return 0;
  else {
    // val f = list.first;
    // val r = list.rest;
    return <mark>list.first</mark> + sum(<mark>list.rest</mark>);
  }
}
</code></pre>

* We can arbitrarily substitute the expression with their value here
* We <strong>are</strong> functional programming

---

