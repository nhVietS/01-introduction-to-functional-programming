## Overview { data-transition="fade none" }

<div style="text-align:left; font-size:x-large">This course will cover four distinct and important questions</div>

1. What does Functional Programming mean?
2. What does it mean to use Functional Programming?
3. How do I learn to use Functional Programming?
4. How do I solve problems using Functional Programming?

<aside class="notes">
  * There are four overall aims for the course
  * Each are addressed discretely
  * This lecture covers the first and second
</aside>

---

