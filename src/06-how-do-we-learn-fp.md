## How do we learn Functional Programming? { data-transition="fade none" }

---

## How do we learn Functional Programming? { data-transition="fade none" }

* To begin learning functional programming, we need a tool in which we can express functional programming concepts
* This tool should ideally <em>minimise barriers</em> to expression of our learned functional programming concepts
* This tool should make efforts to resolve any mismatch between our expressed concepts and our computer hardware 
* This tool should minimise any extraneous effort in our learning journey

---

## How do we learn Functional Programming? { data-transition="fade none" }

###### Introducing the Haskell programming language

* The Haskell programming language is a syntax that is compiled with a Haskell compiler
* The [Glasgow Haskell Compiler (GHC)](http://haskell.org/ghc)

---

## How do we learn Functional Programming? { data-transition="fade none" }

###### Introducing the Haskell programming language

* Haskell <strong>enforces</strong> the commitment to functional programming
* That is, using Haskell, it is <em>not possible</em> to violate reference transparency

---

## How do we learn Functional Programming? { data-transition="fade none" }

###### The GHC Read-Eval-Print-Loop

* GHC includes a REPL for interactively writing Haskell programs
* You can start the REPL by typing <code>ghci</code> in a terminal

<div style="text-align:center">
  <a href="images/loading-ghci.gif">
    <img src="images/loading-ghci.gif" alt="Loading GHCi" style="width:640px"/>
  </a>
</div>

---

## How do we learn Functional Programming? { data-transition="fade none" }

###### The GHC Read-Eval-Print-Loop

* You can also <code>:load</code> and <code>:reload</code> source files in GHCi
* <code>:l</code> and <code>:r</code> for short
* If there is a syntax error, or a type error, GHCi will report the details

<div style="text-align:center">
  <a href="images/reloading-in-ghci.gif">
    <img src="images/reloading-in-ghci.gif" alt="Reloading in GHCi" style="width:640px"/>
  </a>
</div>

---

## How do we learn Functional Programming? { data-transition="fade none" }

###### Haskell syntax

* Function declarations are written with:
  * The function name
  * The function argument list
  * The <code>=</code> character
  * The return value

---

## How do we learn Functional Programming? { data-transition="fade none" }

###### Haskell syntax

<hr>

<pre><code class="language-haskell hljs" data-trim data-noescape>
func x y = (x + y) * 2
</code></pre>

<div style="text-align:center">A function declaration</div>

---

## How do we learn Functional Programming? { data-transition="fade fade" }

###### Haskell syntax

<hr>

<pre><code class="language-haskell hljs" data-trim data-noescape>
<mark>func</mark> x y = (x + y) * 2
</code></pre>

<div style="text-align:center">The function name</div>

---

## How do we learn Functional Programming? { data-transition="fade fade" }

###### Haskell syntax

<hr>

<pre><code class="language-haskell hljs" data-trim data-noescape>
func <mark>x y</mark> = (x + y) * 2
</code></pre>

<div style="text-align:center">The function argument list</div>

---

## How do we learn Functional Programming? { data-transition="fade fade" }

###### Haskell syntax

<hr>

<pre><code class="language-haskell hljs" data-trim data-noescape>
func x y <mark>=</mark> (x + y) * 2
</code></pre>

<div style="text-align:center">The <code>=</code> character</div>

---

## How do we learn Functional Programming? { data-transition="fade fade" }

###### Haskell syntax

<hr>

<pre><code class="language-haskell hljs" data-trim data-noescape>
func x y = <mark>(x + y) * 2</mark>
</code></pre>

<div style="text-align:center">The return value</div>
