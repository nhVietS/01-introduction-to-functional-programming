## Summary { data-transition="fade none" }

* Functional Programming is a commitment to preserving referential transparency of expressions
* Haskell enforces this commitment
* Install the Glasgow Haskell Compiler [http://haskell.org/ghc](http://haskell.org/ghc) on your laptop
* Experiment with GHC by writing Haskell functions in a source file

---

## Introduction to Functional Programming { data-transition="fade none" }

###### Next: The Haskell Syntax
