## What does Functional Programming mean? { data-transition="fade none" }

* Computer Programming or just programming, is the process of designing and building an executable computer program, to achieve a specific computer task
* <em>Functional</em> Programming is a specific type of programming &mdash; of designing and building an executable computer program

---

## What does Functional Programming mean? { data-transition="fade none" }

* Functional Programming is programming, while also adhering to one specific constraint when designing and building computer programs
* Functional Programming can be described as a <em>thesis</em> or <em>commitment</em> to that constraint, while programming

<aside class="notes">
  * It is important to emphasise that Functional Programming is not a property of programming <em>languages</em>, but of programming and subsequently, programs themselves.
  * However, programming languages provide various degrees of support in achieving this commitment.
</aside>

---

## What does Functional Programming mean? { data-transition="fade none" }

* Let's define and discuss this additional constraint that applies to Functional Programming.
* First, let's look at programming without any constraints.

---

## What does Functional Programming mean? { data-transition="fade none" }

###### Suppose the following program:

<pre><code data-trim data-noescape class="lang-java">
int bobble(int a, int b) {
  printf("my favourite number is: %d", a);
  return (a + b) * 2;
}
&hellip;
/* arbitrary code */
wubble(bobble(x, y), bobble(x, y));
</code></pre>

---

## What does Functional Programming mean? { data-transition="fade none" }

###### We make an observation:

<pre><code data-trim data-noescape class="lang-java">
int bobble(int a, int b) {
  printf("my favourite number is: %d", a);
  return (a + b) * 2;
}
&hellip;
/* arbitrary code */
wubble(<mark>bobble(x, y)</mark>, <mark>bobble(x, y)</mark>);
</code></pre>

<aside class="notes">
  * These two sub-expressions are identical
</aside>

---

## What does Functional Programming mean? { data-transition="fade none" }

###### We seek to factor out the common sub-expressions

<pre><code data-trim data-noescape class="lang-java">
int bobble(int a, int b) {
  printf("my favourite number is: %d", a);
  return (a + b) * 2;
}
&hellip;
/* arbitrary code */
<mark>int r = bobble(x, y);</mark>
wubble(<mark>r</mark>, <mark>r</mark>);
</code></pre>

---

## What does Functional Programming mean? { data-transition="fade none" }

###### However &hellip;

<pre><code data-trim data-noescape class="lang-java">
int bobble(int a, int b) {
  printf("my favourite number is: %d", a);
  return (a + b) * 2;
}
&hellip;
/* arbitrary code */
wubble(<mark>bobble(x, y)</mark>, <mark>bobble(x, y)</mark>);
</code></pre>

<pre><code data-trim data-noescape class="lang-java">
int bobble(int a, int b) {
  printf("my favourite number is: %d", a);
  return (a + b) * 2;
}
&hellip;
/* arbitrary code */
<mark>int r = bobble(x, y);</mark>
wubble(<mark>r</mark>, <mark>r</mark>);
</code></pre>

<aside class="notes">
  * The refactoring has resulted in two different programs.
  * The first program prints <code>"hi"</code> twice, while the second program prints <code>"hi"</code> only once.
</aside>

---

## What does Functional Programming mean? { data-transition="fade none" }

###### Common sub-expressions cannot be na&iuml;vely factored

* Our refactoring has resulted in two different programs
* The first program prints <code>"hi"</code> twice, while the second program, after refactoring, prints <code>"hi"</code> only once.

---

## What does Functional Programming mean? { data-transition="fade none" }

###### Let's look at a different example:

<pre><code data-trim data-noescape class="lang-java">
int bibble(int a, int b) {
  return (a + b) * 2;
}
&hellip;
/* arbitrary code */
wubble(bibble(x, y), bibble(x, y));
</code></pre>

---

## What does Functional Programming mean? { data-transition="fade none" }

###### We make a similar observation:

<pre><code data-trim data-noescape class="lang-java">
int bibble(int a, int b) {
  return (a + b) * 2;
}
&hellip;
/* arbitrary code */
wubble(<mark>bibble(x, y)</mark>, <mark>bibble(x, y)</mark>);
</code></pre>

---

## What does Functional Programming mean? { data-transition="fade none" }

###### We factor out the common sub-expressions:

<pre><code data-trim data-noescape class="lang-java">
int bibble(int a, int b) {
  return (a + b) * 2;
}
&hellip;
/* arbitrary code */
<mark>int r = bibble(x, y);</mark>
wubble(<mark>r</mark>, <mark>r</mark>);
</code></pre>

---

## What does Functional Programming mean? { data-transition="fade none" }

###### We have preserved the equivalence of the two programs

<pre><code data-trim data-noescape class="lang-java">
int bibble(int a, int b) {
  return (a + b) * 2;
}
&hellip;
/* arbitrary code */
wubble(<mark>bibble(x, y)</mark>, <mark>bibble(x, y)</mark>);
</code></pre>

<pre><code data-trim data-noescape class="lang-java">
int bibble(int a, int b) {
  return (a + b) * 2;
}
&hellip;
/* arbitrary code */
<mark>int r = bibble(x, y);</mark>
wubble(<mark>r</mark>, <mark>r</mark>);
</code></pre>

---

## What does Functional Programming mean? { data-transition="fade none" }

* In the first case, our refactoring resulted in two different programs
* While in the second case, our refactoring resulted in two equivalent programs

---

## What does Functional Programming mean? { data-transition="fade none" }

* Functional Programming is a commitment to <strong>always</strong> achieving this latter property of our program expressions
* Specifically, we commit to preserving the ability to substitute common expressions with a value, and <em>without changing the program behaviour</em>

---

## What does Functional Programming mean? { data-transition="fade none" }

* Or in other words, when we use <code>=</code> in a program, such as <code>x = expression</code>, we truly mean it
* the expression and its value (<code>x</code>), <strong>are equal</strong>
* If an expression can be substituted with its value, without changing the program behaviour, we say that expression is <q>referentially transparent</q>

---

## What does Functional Programming mean? { data-transition="fade none" }

<q>Functional Programming is a thesis, or commitment, to always achieving referential transparency of program expressions</q>

---

## What does Functional Programming mean? { data-transition="fade none" }

<pre><code data-trim data-noescape class="lang-java">
int bobble(int a, int b) {
  printf("my favourite number is: %d", a);
  return (a + b) * 2;
}
</code></pre>

* Any expression that calls <code>bobble</code> will violate referential transparency
* This style of programming is <em>not</em> consistent with Functional Programming

---

## What does Functional Programming mean? { data-transition="fade none" }

<pre><code data-trim data-noescape class="lang-java">
int bibble(int a, int b) {
  return (a + b) * 2;
}
</code></pre>

* <code>bibble</code> is a function
* Calling <code>bibble</code> will not violate referential transparency
* This style of programming is consistent with Functional Programming

