## Why do we do Functional Programming? { data-transition="fade none" }

---

## Why do we do Functional Programming? { data-transition="fade none" }

###### Programming in this style has some benefits

---

## Why do we do Functional Programming? { data-transition="fade none" }

###### Reasoning about our programs

* Computer programs are made up of discrete <em>sub-programs</em>
* This means we can <em>reason</em> about behaviour of each program part, which can be broken down, or assembled up
* This is contrast to reasoning about a computer program as an entire, interdependent unit

---

## Why do we do Functional Programming? { data-transition="fade none" }

###### Composition of programs

* We can also utilise the concept of <em>program composition</em>
* Since our computer programs are discrete units, they perform a single task, which can be assembled to another program, producing less trivial program tasks, and so on

---

## Why do we do Functional Programming? { data-transition="fade none" }

###### Abstraction

* We can produce units of abstraction and trust that they achieve one goal &mdash; that of the abstraction and importantly, <em> they do nothing else</em>
* The end goal of utilising abstraction is to take many similar programs, and turn them into one program, that achieves the same goal of the many programs

---

## Why do we do Functional Programming? { data-transition="fade none" }

###### There are some penalties associated with functional programming

---

## Why do we do Functional Programming? { data-transition="fade none" }

###### Penalties

* There exists a mismatch between our (functional) programming style and the nature of existing computer hardware
* This necessitates a sufficiently well-designed <em>compiler</em> that can resolve this mismatch
* Otherwise, our computer programs may become unacceptably slow, or overflow the function call stack

---

## Why do we do Functional Programming? { data-transition="fade none" }

* The activity of <em>learning functional programming</em> is all about exploring and internalising these benefits and penalties
* We do this learning by practice of solving programming problems, utilising new tools to solve them, and subsequent repetition of this practice

---

