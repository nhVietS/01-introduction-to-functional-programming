## Aims { data-transition="fade none" }

<div style="text-align:left; font-size:x-large">At the end of this lecture, the student should be able to:</div>
* Articulate a concrete meaning for Functional Programming
* Describe the conditions under which we may or may not be, using Functional Programming

<aside class="notes">
  * The subsequent questions are covered in later lectures
</aside>

---

